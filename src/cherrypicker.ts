export type HasId<A, IdKey extends keyof A> = A[IdKey] extends string
  ? { [key in IdKey as key]: string }
  : never

export type ArrayGenerator<A> = (seed: A) => Array<A>

export type AsyncArrayGenerator<A> = (seed: A) => Promise<Array<A>>

export type IsArrayTree<A, SubtreeKey extends keyof A> = A[SubtreeKey] extends
  | undefined
  | Array<A>
  ? { [key in SubtreeKey as key]?: Array<A> }
  : never

export type IsGeneratorTree<
  A,
  SubtreeKey extends keyof A,
> = A[SubtreeKey] extends undefined | ArrayGenerator<A>
  ? { [key in SubtreeKey as key]?: ArrayGenerator<A> }
  : never

export type IsAsyncTree<A, SubtreeKey extends keyof A> = A[SubtreeKey] extends
  | undefined
  | AsyncArrayGenerator<A>
  ? { [key in SubtreeKey as key]?: AsyncArrayGenerator<A> }
  : never

export type IsTree<A, SubtreeKey extends keyof A> =
  | IsArrayTree<A, SubtreeKey>
  | IsGeneratorTree<A, SubtreeKey>
  | IsAsyncTree<A, SubtreeKey>

export type IsPickable<
  A,
  IdKey extends keyof A,
  SubtreeKey extends keyof A,
> = HasId<A, IdKey> & IsTree<A, SubtreeKey>

export type IsThinable<
  A,
  IdKey extends keyof A,
  SubtreeKey extends keyof A,
> = HasId<A, IdKey> & IsArrayTree<A, SubtreeKey>

export type CherryPicker<
  Tree extends IsPickable<Tree, IdKey, SubtreeKey>,
  IdKey extends keyof Tree = keyof Tree,
  SubtreeKey extends keyof Tree = keyof Tree,
> = {
  pick: (roots: Tree | Tree[], path: string[]) => Promise<Tree | undefined>
}

export type ThinTree = {
  id: string
  subtree: ThinTree[]
}

export async function fetchChildren<
  Tree extends IsTree<Tree, SubtreeKey>,
  SubtreeKey extends keyof Tree = keyof Tree,
>(root: Tree, subtreeKey: SubtreeKey): Promise<Tree[]> {
  const children = root[subtreeKey]

  if (!children) {
    return []
  }

  if (Array.isArray(children)) {
    return children
  }

  return await children(root)
}

// export async function reify<
//   Tree extends IsTree<Tree, SubtreeKey>,
//   SubtreeKey extends keyof Tree = keyof Tree,
// >(root: Tree, subtreeKey: SubtreeKey, maxDepth: number) {
//   if (maxDepth >= 1) {
//     const children = await fetchChildren(root, subtreeKey)
//     const reifiedChildren = await Promise.all(
//       children.map((child) => reify(child, subtreeKey, maxDepth - 1)),
//     )
//     return { ...root, [subtreeKey]: reifiedChildren }
//   }

//   return { ...root, [subtreeKey]: [] }
// }

export async function pick<
  Tree extends IsPickable<Tree, IdKey, SubtreeKey>,
  IdKey extends keyof Tree = keyof Tree,
  SubtreeKey extends keyof Tree = keyof Tree,
>(
  roots: Tree[],
  idKey: IdKey,
  subtreeKey: SubtreeKey,
  path: string[],
): Promise<Tree | undefined> {
  if (roots.length === 0 || path.length === 0) {
    return undefined
  }

  const rootKey = path[0]
  const rootNode = roots.find((root) => root[idKey] === rootKey)

  if (!rootNode || path.length === 1) {
    return rootNode
  }

  const children = await fetchChildren(rootNode, subtreeKey)

  return pick(children, idKey, subtreeKey, path.slice(1))
}

export function thin<
  Tree extends IsThinable<Tree, IdKey, SubtreeKey>,
  IdKey extends keyof Tree = keyof Tree,
  SubtreeKey extends keyof Tree = keyof Tree,
>(roots: Tree[], idKey: IdKey, subtreeKey: SubtreeKey): ThinTree[] {
  return roots.map((root) => ({
    id: root[idKey],
    subtree: thin(root[subtreeKey], idKey, subtreeKey),
  }))
}

function asArray<A>(a: A | A[]): A[] {
  return Array.isArray(a) ? a : [a]
}

export function cherrypicker<
  Tree extends IsPickable<Tree, IdKey, SubtreeKey>,
  IdKey extends keyof Tree = keyof Tree,
  SubtreeKey extends keyof Tree = keyof Tree,
>(
  typeHint: Tree | (new () => Tree),
  idKey: IdKey,
  subtreeKey: SubtreeKey,
): CherryPicker<Tree, IdKey, SubtreeKey> {
  return {
    pick(roots: Tree | Tree[], path: string[]) {
      return pick(asArray(roots), idKey, subtreeKey, path)
    },
  }
}
