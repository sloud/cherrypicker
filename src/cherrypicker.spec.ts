import { cherrypicker } from './cherrypicker'

const orThrow = () => {
  throw new Error('error')
}

interface Node<A> {
  id: string
  value: A
  nodes?: Node<A>[]
}

const root: Node<number> = {
  id: '0',
  value: 0,
  nodes: [
    {
      id: '1',
      value: 1,
      nodes: [
        {
          id: '3',
          value: 3,
          // undefined children
        },
      ],
    },
    {
      id: '2',
      value: 2,
      nodes: [
        {
          id: '4',
          value: 4,
          nodes: [],
        },
      ],
    },
  ],
}

describe('cherrypicker', () => {
  const picker = cherrypicker(root, 'id', 'nodes')

  describe('pick', () => {
    it('should return undefined for empty [] args', () => {
      expect(picker.pick([], [])).resolves.toBeUndefined()
    })

    it('should should return a node when path matches', () => {
      const node = root.nodes?.[0]?.nodes?.[0] as {}

      expect(picker.pick(root, ['0', '1', '3'])).resolves.toMatchObject(node)
    })

    it('should should return `undefined` when path does not match', () => {
      expect(picker.pick(root, ['0', '1', '2'])).resolves.toBeUndefined()
    })
  })
})
